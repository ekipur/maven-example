package paket.b;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.net.URL;

/**
 * Class for Module B
 *
 * @author ekipur
 */
public class SubModB {

  private String inhalt = "Inhalt nicht verfügbar!";

  /**
   * Constructor.
   */
  public SubModB() {
    inhalt();
  }

  private void inhalt() {
    URL url = getClass().getClassLoader().getResource("paket/resources/text.datei");
    if (url != null) {
      try (BufferedInputStream bis = new BufferedInputStream(url.openStream())) {
        inhalt = new String(bis.readAllBytes());
      } catch (IOException x) {
        System.out.println("Fehler beim Einlesen der Zugabe: " + x);
      }
    }
  }

  @Override
  public String toString() {
    return getClass().getSimpleName() + ": " + inhalt;
  }
}
