package paket.main;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import paket.a.SubModA;
import paket.b.SubModB;

/**
 * Main class - Contains main method for simple System.out.println.
 *
 * @author ekipur
 */
public class ModMain {

  /**
   * Logger
   */
  public static Logger LOGGER = LoggerFactory.getLogger("mod");

  /**
   * Main method
   *
   * @param args Arguments for main method
   */
  public static void main(String[] args) {
    ModMain m = new ModMain();
    m.ausgabe();
  }

  private void ausgabe() {
    System.out.println(ModMain.class.getSimpleName());

    for (Object mod : new Object[]{
      new SubModA(),
      new SubModB()
    }) {
      LOGGER.info(mod.toString());
    }
  }
}
