/** Main module */
module java.modul.main {
  requires java.modul.a;
  requires java.modul.b;

  requires org.slf4j;
  requires org.slf4j.simple;

  exports paket.main;
}
