package paket.a;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class TestSubModA
{
  @Test
  public void submoda()
  {
    SubModA a = new SubModA();
    Assertions.assertEquals(a.toString(), SubModA.class.getSimpleName());
  }
}
