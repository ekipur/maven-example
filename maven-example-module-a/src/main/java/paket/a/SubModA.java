package paket.a;

/**
 * Class for Module A
 *
 * @author ekipur
 */
public class SubModA {

  @Override
  public String toString() {
    return getClass().getSimpleName();
  }
}
